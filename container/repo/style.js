import { StyleSheet } from 'react-native';

/**
 * stylesheet for repository page or container
 */
const Style = StyleSheet.create({
    font: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 20,
        fontFamily: "Bradley Hand"
    },
    listView: {
        margin: 20
    },
    listItem: {
        borderRadius: 10,
        borderWidth: 1,
        margin: 10
    }
});


export default Style