import React, { Component } from 'react';
import * as R from 'react-native';
import Style from "./style"
import { connect } from "react-redux"
import { bindActionCreators } from 'redux';
import * as repoAction from "../../redux/repo/repoReducer"
import * as modalAction from "../../redux/modal/modalReducer"
import TouchableScale from 'react-native-touchable-scale';
import { Header, ListItem } from "react-native-elements"
import Modal from "../../component/modal/index"

/**
 * this is the repository page or container which connect with redux
 */
class Repo extends Component {
    state = {
        showModal: false,
    }

    componentWillMount() {
        this.props.getRepo()
    }

    //close modal event handler
    handleCloseModal = () => {
        this.setState({ showModal: false })
    }

    //item list press event handler
    handlePress = (l) => {
        this.setState({ showModal: true })
        this.props.updateModal(l)
    }

    //modal view button event handler
    handleViewRepo = () => {
        const url = this.props.modal.html_url
        R.Linking.openURL(url)
    }

    //main render function
    render() {
        // console.log(this.props.repo)
        const { showModal } = this.state
        return (
            <R.View>
                <R.ScrollView stickyHeaderIndices={[0]}>
                    <Header
                        centerComponent={{ text: 'Repository', style: { ...Style.font } }}
                        containerStyle={{
                            backgroundColor: '#24292E',
                            justifyContent: 'space-around',
                        }}
                    />
                    <R.View style={{ ...Style.listView }}>
                        {
                            this.props.repo.map((l, i) => (
                                <ListItem
                                    Component={TouchableScale}
                                    friction={10} //
                                    tension={200} // These props are passed to the parent component (here TouchableScale)
                                    activeScale={0.95} //
                                    linearGradientProps={{
                                        colors: ['#418B3E', '#64CF58'],
                                        start: [0, 1],
                                        end: [0, 0.2],
                                    }}
                                    key={i}
                                    title={l.name}
                                    // leftIcon={<Icon name='github' size={30} />}
                                    leftAvatar={{ rounded: true, source: { uri: l.owner.avatar_url } }}
                                    bottomDivider
                                    titleStyle={{ color: 'white', fontWeight: 'bold' }}
                                    subtitleStyle={{ color: 'white', fontSize: 8 }}
                                    subtitle={l.owner.login}
                                    chevron={{ color: 'white' }}
                                    contentContainerStyle={{ borderRadius: 20 }}
                                    onPress={() => this.handlePress(this.props.repo[i])}
                                    containerStyle={{ ...Style.listItem }}
                                />
                            ))
                        }
                        <Modal
                            showModal={showModal}
                            handleCloseModal={this.handleCloseModal}
                            data={this.props.modal}
                            handleViewRepo={this.handleViewRepo}
                        />
                    </R.View>
                </R.ScrollView>
            </R.View>
        );
    }
}

//redux magic, map state to props
const mapStateToProps = (state) => {
    return {
        gitUser: state.git,
        repo: state.repo,
        modal: state.modal
    };
};

//redux magic, map action to props
const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({
        ...repoAction,
        ...modalAction
    }, dispatch),
});

//this is HOC
export default connect(mapStateToProps, mapDispatchToProps)(Repo);