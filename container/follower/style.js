import { StyleSheet } from 'react-native';

/**
 * stylesheet for the follower page
 */
const Style = StyleSheet.create({
    font: {
        color: '#fff', fontWeight: 'bold', fontSize: 20, fontFamily: "Bradley Hand"
    }
});


export default Style