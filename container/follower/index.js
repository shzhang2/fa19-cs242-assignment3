import React, { Component } from 'react';
import * as R from 'react-native';
import { Header, Avatar } from "react-native-elements"
import Style from "./style"
import { connect } from "react-redux"
import { bindActionCreators } from 'redux';
import * as gitAction from "../../redux/profile/gitReducer"

/**
 * this is the follower page or container which connect with redux
 */
class Follower extends Component {
    state = {}
    render() {
        return (
            <R.View>
                <Header
                    centerComponent={{ text: 'Followers', style: { ...Style.font } }}
                    containerStyle={{
                        backgroundColor: '#1DA1F2',
                        justifyContent: 'space-around',
                    }}
                />
            </R.View>
        );
    }
}

//redux magic, map state to props
const mapStateToProps = (state) => {
    return {
        git: state.git,
    };
};

//redux magic, map action to props
const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({
        gitAction
    }, dispatch),
});

//this is HOC
export default connect(mapStateToProps, mapDispatchToProps)((Follower));