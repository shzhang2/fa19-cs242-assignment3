import { StyleSheet } from 'react-native';

/**
 * stylesheet for profile page or container
 */
const Style = StyleSheet.create({
    hearder: {
        backgroundColor: '#78909C',
        justifyContent: 'space-around',
    },
    font: {
        color: '#fff', fontWeight: 'bold', fontSize: 20, fontFamily: "Bradley Hand"
    },
    image: {
        width: 120,
        height: 120,
        borderWidth: 2,
        borderRadius: 60,
        borderColor: "white"
    },
    backgroundImage: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        overflow: "hidden",
        marginTop: 5,
    },
    container: {
        alignItems: 'center',
    },
    namePosition: {
        justifyContent: 'center',
        margin: 20
    },
    nameText: {
        fontSize: 20,
        fontFamily: "Trebuchet MS",
        color: "white",
    },
    info: {
        margin: 30
    },
    buttonGroup: {
        height: 40,
    },
    textareaContainer: {
        height: 180,
        padding: 5,
        backgroundColor: '#F5FCFF',
    },
    textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
    },
    createDate: {
        textAlign: "right",
        marginTop: 10,
        color: "#5D4037"
    }
});


export default Style