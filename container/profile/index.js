import React, { Component } from 'react';
import * as R from 'react-native';
import { Header, Avatar, ButtonGroup, ListItem } from "react-native-elements"
import { Col, Row, Grid } from "react-native-easy-grid";
import Info from "../../component/Info/index"
import TextArea from "../../component/textInput/index"
import Carousel from 'react-native-snap-carousel';
import { connect } from "react-redux"
import { bindActionCreators } from 'redux';
import * as gitAction from "../../redux/profile/gitReducer"
import Style from "./style"
import _ from "lodash"
import Icon from "react-native-vector-icons/FontAwesome"

/**
 * this is the profile page or container which connect with redux
 */
class Profile extends Component {
    state = {
        buttonIndex: 0
    }
    //button group 
    buttons = [
        'Username',
        'Website',
        'Email',
    ]
    //item list 
    list = [
        {
            title: 'Repository',
            icon: "devices",
        },
        {
            title: 'Follower',
            icon: 'thumb-up',
        },
        {
            title: 'Following',
            icon: 'star',
        }
    ]

    componentWillMount() {
        this.props.getGitData()
    }

    //button click event handler
    handleOnClick = (index) => {
        this.setState({ buttonIndex: index })
    }

    //item list click event handler
    handleNavigate = (i) => {
        const { navigate } = this.props.navigation
        navigate(this.list[i].title)
    }

    //render item list badge
    getListItemContent = (l) => {
        const { gitUser } = this.props
        if (l.title === "Repository") {
            return gitUser.public_repos
        }
        else if (l.title === "Follower") {
            return gitUser.followers
        }
        else {
            return gitUser.following
        }
    }

    //render basic info tags, editable is false
    renderBasicInfo = (buttonIndex) => {
        const { gitUser } = this.props
        if (buttonIndex === 0) {
            return <Info label="Github Username" value={gitUser.login} editable={false} />
        }
        else if (buttonIndex === 1) {
            return <Info label="Website" value={gitUser.blog} editable={false} />
        }
        else {
            const value = gitUser.email ? gitUser.email : "N/A"
            return <Info label="Email" value={value} editable={false} />
        }
    }

    //main render function
    render() {
        const { buttonIndex } = this.state
        const { gitUser } = this.props
        const date = gitUser.created_at.split("T")
        console.log(gitUser)
        return (
            <Grid>
                <Col>
                    <R.ScrollView stickyHeaderIndices={[0]}>
                        <Header
                            centerComponent={{ text: 'Profile', style: { ...Style.font } }}
                            containerStyle={{ ...Style.hearder }}
                        />
                        <R.ImageBackground source={(require("./background3.jpg"))} style={{ ...Style.backgroundImage }} resizeMode="cover">
                            <Col>
                                <Row style={{ justifyContent: 'center', alignItems: "flex-end", height: 250 }}>
                                    <Avatar
                                        size="xlarge"
                                        rounded
                                        title="Z"
                                        activeOpacity={0.7}
                                        source={{ uri: gitUser.avatar_url, }}
                                        showEditButton
                                        style={{
                                            ...Style.image,
                                        }}
                                        onEditPress={() => { console.log("icon") }}
                                        onLongPress={() => { R.Linking.openURL(gitUser.html_url) }}
                                    />
                                </Row>
                                <Row style={{ ...Style.namePosition }}>
                                    <R.Text style={{ ...Style.nameText }}>{gitUser.name}</R.Text>
                                </Row>
                            </Col>
                        </R.ImageBackground>
                        <R.View>
                            {
                                this.list.map((l, i) => (
                                    <ListItem
                                        key={i}
                                        title={l.title}
                                        leftIcon={{ name: l.icon }}
                                        bottomDivider
                                        chevron
                                        onPress={() => this.handleNavigate(i)}
                                        badge={{ value: (this.getListItemContent(l)), textStyle: { color: 'white' }, containerStyle: {} }}
                                    />
                                ))
                            }
                        </R.View>
                        <R.View style={{ ...Style.info }} >
                            <ButtonGroup
                                onPress={this.handleOnClick}
                                selectedIndex={buttonIndex}
                                buttons={this.buttons}
                                containerStyle={{ ...Style.buttonGroup }}
                            />
                            {this.renderBasicInfo(buttonIndex)}
                            <TextArea
                                multiline={true}
                                numberOfLines={400}
                                defaultValue={gitUser.bio}
                                editable={false}
                            />
                            <R.View>
                                <R.Text style={{ ...Style.createDate }}>
                                    <Icon name='github' size={22} />
                                    {'\xa0\xa0'}Since {date[0]}
                                </R.Text>
                            </R.View>
                        </R.View>
                    </R.ScrollView>
                </Col>
            </Grid>

        );
    }
}

//redux magic, map state to props
const mapStateToProps = (state) => {
    return {
        gitUser: state.git,
    };
};

//redux magic, map action to props
const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({
        ...gitAction
    }, dispatch),
});


//this is HOC
export default connect(mapStateToProps, mapDispatchToProps)(Profile);