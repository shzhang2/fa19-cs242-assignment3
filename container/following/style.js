import { StyleSheet } from 'react-native';

/**
 * style sheet for following page or container
 */
const Style = StyleSheet.create({
    font: {
        color: '#fff', fontWeight: 'bold', fontSize: 20, fontFamily: "Bradley Hand"
    }
});


export default Style