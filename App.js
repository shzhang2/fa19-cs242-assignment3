import React, { Component } from 'react';
import store from "./redux/app"
import { Provider } from "react-redux"
import Stage1 from "./Stage1"

/**
 * the main app. 
 * have to be done in this way because we have to use provider to give containers access to the store
 */
class App extends Component {
  state = {}
  render() {
    return (
      <Provider store={store}>
        <Stage1 />
      </Provider>
    );
  }
}

export default App;

// const App = require("./storybook")
// export default App;

