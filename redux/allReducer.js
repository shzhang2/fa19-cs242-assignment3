import { combineReducers } from "redux";
import gitUserReducer from "./profile/gitReducer";
import routeReducer from "./route/index"
import repoReducer from "./repo/repoReducer"
import modalReducer from "./modal/modalReducer"

/**
 * redux magic, combine all reducers for creating store purpose
 */
const allReducers = combineReducers({
    git: gitUserReducer,
    router: routeReducer,
    repo: repoReducer,
    modal: modalReducer
});

export default allReducers;