
const initRepo = []

//updateModal Action
function updateModalAction(data) {
    return {
        type: "UPDATE_MODAL",
        payload: data
    };
}

//update Modal action creator
export function updateModal(l) {
    return async (dispatch, getState) => {
        try {
            dispatch(updateModalAction(l));
        }
        catch (e) {
            console.log(e)
        }
    };
}

//modal reducer
function modalReducer(state = initRepo, action) {
    switch (action.type) {
        case "UPDATE_MODAL":
            return action.payload;
        default:
            return state;
    }
}

export default modalReducer;