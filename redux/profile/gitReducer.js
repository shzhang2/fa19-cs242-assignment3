import userService from "../../service/user/index"

//set initial state
const initUser = {
    login: "",
    id: 1,
    node_id: "=",
    avatar_url: undefined,
    gravatar_id: "",
    url: "",
    html_url: "",
    followers_url: "",
    following_url: "",
    gists_url: "",
    starred_url: "",
    subscriptions_url: "",
    organizations_url: "",
    repos_url: "",
    events_url: "",
    received_events_url: "",
    type: "",
    site_admin: false,
    name: "",
    company: "",
    blog: "",
    location: "",
    email: "N/A",
    hireable: false,
    bio: "",
    public_repos: 0,
    public_gists: 0,
    followers: 0,
    following: 0,
    created_at: "",
    updated_at: "",
    plan: {}
}

//get user action 
function getUserAction(data) {
    return {
        type: "GET_DATA",
        payload: data
    };
}

//get user action creator
export function getGitData() {
    return async (dispatch, getState) => {
        try {
            const result = await userService.getUser()
            dispatch(getUserAction(result));
        }
        catch (e) {
            console.log(e)
        }
    };
}

//github user reducer
function gitUserReducer(state = initUser, action) {
    switch (action.type) {
        case "GET_DATA":
            return action.payload;

        default:
            return state;
    }
}

export default gitUserReducer;