import gitService from "../../service/user/index"

const initRepo = []

//get repo action
function getRepoAction(data) {
    return {
        type: "GET_REPO",
        payload: data
    };
}

//get repo action creator
export function getRepo() {
    return async (dispatch, getState) => {
        try {
            const result = await gitService.getRepo()
            dispatch(getRepoAction(result));
        }
        catch (e) {
            console.log(e)
        }
    };
}

//github repo reducer
function gitRepoReducer(state = initRepo, action) {
    switch (action.type) {
        case "GET_REPO":
            return action.payload;
        default:
            return state;
    }
}

export default gitRepoReducer;