import React, { Component } from 'react';
import { createStore, applyMiddleware } from "redux"
import thunk from "redux-thunk";
import allReducers from "./allReducer.js"

/**
 * create the store and apply redux-thunk middleware
 */
const makeStore = () => createStore(
    allReducers,
    applyMiddleware(thunk)
)
const store = makeStore()


export default store