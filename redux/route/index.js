
/**
 * THIS MODULE HAVENT BEEN USED YET
 * JUST IN CASE THE FUTURE NEEDED
 */

//update route index or url action
function updateIndexAction(index) {
    return {
        type: "UPDATE_INDEX",
        payload: index
    };
}

// router action creator
export function updateIndex(index) {
    return async (dispatch, getState) => {
        try {
            dispatch(updateIndexAction(index));
        }
        catch (e) {
            console.log(e)
        }
    };
}

//router reducer
function routeReducer(state = 0, action) {
    switch (action.type) {
        case "UPDATE_INDEX":
            return action.payload;

        default:
            return state;
    }
}

export default routeReducer;