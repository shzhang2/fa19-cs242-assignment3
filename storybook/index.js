import { AppRegistry } from 'react-native';
import { getStorybookUI, configure } from '@storybook/react-native';
import './rn-addons';

/**
 * import stories
 */
configure(() => {
    require('./stories/index');
}, module);
const StorybookUIRoot = getStorybookUI({});

//create the story book
AppRegistry.registerComponent('App', () => StorybookUIRoot);

export default StorybookUIRoot;
