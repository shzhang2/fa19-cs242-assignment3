import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from "react-native-vector-icons/FontAwesome"
import Profile from "./container/profile/index"
import Repo from "./container/repo/index"
import Follower from "./container/follower/index"
import Following from "./container/following/index"
import * as gitAction from "./redux/profile/gitReducer"
import { connect } from "react-redux"
import { bindActionCreators } from 'redux';

/**
 * this is an UI component in order to navigate to different page
 * @param {*} props  props
 */
const TabNavigator = (props) => {
    return createMaterialBottomTabNavigator(
        {
            Profile: {
                screen: Profile,
                navigationOptions: {
                    tabBarLabel: "Profile",
                    tabBarColor: "#78909C",
                    tabBarIcon: ({ focused, horizontal, tintColor }) => {
                        return (
                            <Icon name='user-circle-o' color={tintColor} size={25} />
                        )
                    }
                }
            },
            Repository: {
                screen: Repo,
                navigationOptions: {
                    tabBarLabel: "Repository",
                    tabBarColor: "#24292E",
                    tabBarIcon: ({ focused, horizontal, tintColor }) => {
                        return (
                            <Icon name='github' color={tintColor} size={25} />
                        )
                    }
                },
            },
            Follower: {
                screen: Follower,
                navigationOptions: {
                    tabBarLabel: "Follower",
                    tabBarColor: "#1DA1F2",
                    tabBarIcon: ({ focused, horizontal, tintColor }) => {
                        return (
                            <Icon name='thumbs-o-up' color={tintColor} size={25} />
                        )
                    }
                },
            },
            Following: {
                screen: Following,
                navigationOptions: {
                    tabBarLabel: "Following",
                    tabBarColor: "#00bcd4",
                    tabBarIcon: ({ focused, horizontal, tintColor }) => {
                        return (
                            <Icon name='group' color={tintColor} size={25} />
                        )
                    }
                },
            }
        },
        {
            shifting: true,
            initialRouteName: 'Profile',
            order: [
                "Profile", "Repository", "Follower", "Following"
            ],
            activeColor: '#f0edf6',
            inactiveColor: '#3e2465',
            tabBarOptions: {
                activeTintColor: "red"
            }
        }
    );
}

//redux magic, map state to props
const mapStateToProps = (state) => {
    return {
        git: state.git,
    };
};

//redux magic, map action to props
const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({
        ...gitAction
    }, dispatch),
});

//double HOC
export default connect(mapStateToProps, mapDispatchToProps)(createAppContainer(TabNavigator()));