# WARNING
You probably need to have your hands dirty with redux, react-redux, redux-thunk to read this repo

## Installation
```bash
npm install
```
## Usage
```bash
npm start
```
Install Expo on you phone. And you need to use your phone to scan the QR code 

## Storybook
Storybook is currently not doing well with Expo react-native, will fix later