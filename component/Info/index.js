import React, { Component } from 'react';
import * as R from "react-native"
import Style from "./Style"
import { TextField } from 'react-native-material-textfield';

/**
 * this is supposed to be the input or info component
 * @param {*} props props
 */
const Info = (props) => {
    const label = props.label
    return (
        <R.View style={{ ...Style.textField }}>
            <TextField
                title={label}
                label={props.value}
                editable={props.editable}
                fontSize={props.label === "Website" ? 15 : 25}
                baseColor="#757575"
            />
        </R.View>
    )
}

export default Info