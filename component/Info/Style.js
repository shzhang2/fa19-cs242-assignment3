import { StyleSheet } from 'react-native';

/**
 * style for info component
 */
const Style = StyleSheet.create({
    textField: {
        marginTop: 0,
        marginBottom: 30
    }
});


export default Style