import React, { Component } from 'react';
import * as R from "react-native"
import Style from "./Style"

/**
 * wrapper of the native text input tag
 * @param {*} props  props
 */
const TextArea = (props) => {
    return (
        <R.View>
            <R.TextInput
                style={{ ...Style.textArea }}
                {...props}
            />
        </R.View>
    )
}

export default TextArea