import { StyleSheet } from 'react-native';

/**
 * stylesheet for textinput tag
 */
const Style = StyleSheet.create({
    textArea: {
        height: 200,
        borderColor: '#B0BEC5',
        borderRadius: 5,
        backgroundColor: "#F4F4F4",
        color: "#757575",
        shadowColor: "#7e57c2",
        shadowOpacity: 0.25,
        shadowRadius: 8.51,
    }
});


export default Style