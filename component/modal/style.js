import { StyleSheet } from 'react-native';

/**
 * stylesheet for modal component
 */
const Style = StyleSheet.create({
    button: {
        width: 150,
        margin: 20,
    },
    title: {
        fontSize: 25,
        textAlign: "center",
        marginBottom: 15,
        fontFamily: "Trebuchet MS"
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    }
});


export default Style