import React, { Component } from 'react';
import * as R from "react-native"
import Style from "./style"
import { Overlay, Button } from "react-native-elements"
import Textarea from "../textInput/index"

/**
 * this is the modal whichs replace the naive dialog
 * @param {*} props props
 */
const Modal = (props) => {
    return (
        <Overlay
            isVisible={props.showModal}
            windowBackgroundColor="rgba(148, 151, 153, .5)"
            overlayBackgroundColor="#FFFFFF"
            onBackdropPress={props.handleCloseModal}
            borderRadius={12}

        >
            <R.View>
                <R.Text style={{ ...Style.title }}>{props.data.name}</R.Text>
                <Textarea
                    multiline={true}
                    numberOfLines={400}
                    defaultValue={props.data.description}
                    editable={false}
                />
                <R.View style={{ ...Style.container }}>
                    <Button
                        style={{ ...Style.button }}
                        title="View"
                        onPress={props.handleViewRepo}
                        buttonStyle={{ backgroundColor: "#57B64C" }}
                    />
                    <Button
                        style={{ ...Style.button }}
                        title="Close"
                        onPress={props.handleCloseModal}
                    />
                </R.View>
            </R.View>
        </Overlay>
    )
}

export default Modal