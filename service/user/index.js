import API from "../index";

module.exports = {
    getUser: getUser,
    getRepo: getRepo
};

/**
 * http request. get github user
 */
async function getUser() {
    const result = await API.get("/users/BurNIngDK");
    return result.data;
}

/**
 * http request. get github repo
 */
async function getRepo() {
    const result = await API.get("/users/BurNIngDK/repos")
    return result.data
}