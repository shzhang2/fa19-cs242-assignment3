import axios from "axios";

const client_id = "Iv1.adc66ccf6fa1041f"
const client_secret = "370c9d1fc85d9bdb190bce0b7284a2b7c662efea"

/**
 * create axios instance with base url and allow CORS for different platform
 */
export default axios.create({
    baseURL: `https://api.github.com`,
    headers: { "Access-Control-Allow-Origin": "*" },
    timeout: 5000
});